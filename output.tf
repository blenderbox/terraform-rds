locals {
  dbuser_arn_prefix = "arn:aws:rds-db:${data.aws_region.current.name}:${data.aws_caller_identity.current.id}:dbuser:${aws_db_instance.rds.resource_id}"
}
output "rds_address" {
  value = aws_db_instance.rds.address
}

output "db_access_sg_id" {
  value = aws_security_group.db_access_sg.id
}

output "resource_id" {
  value = aws_db_instance.rds.resource_id
}

output "dbuser_arn_prefix" {
  description = "the rds dbuser arn prefix. Append `/username` to specify user"
  value       = local.dbuser_arn_prefix
}

output "rds_iam_master_user_arn" {
  description = "outputs the RDS master dbuser arn; usible for IAM DB auth policies"
  value       = "${local.dbuser_arn_prefix}/${aws_db_instance.rds.username}"
}

data "aws_caller_identity" "current" {}
data "aws_region" "current" {}