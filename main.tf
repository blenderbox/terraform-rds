/*====
RDS
======*/

/* subnet used by rds */
resource "aws_db_subnet_group" "rds_subnet_group" {
  name        = "${var.environment}-rds-subnet-group-${var.project}"
  description = "RDS subnet group"
  subnet_ids  = var.subnet_ids
}

/* Security Group for resources that want to access the Database */
resource "aws_security_group" "db_access_sg" {
  vpc_id      = var.vpc_id
  name        = "${var.environment}-db-access-sg-${var.project}"
  description = "Allow access to RDS"

}

resource "aws_security_group" "rds_sg" {
  name        = "${var.environment}-rds-sg-${var.project}"
  description = "${var.environment} Security Group"
  vpc_id      = var.vpc_id

  // allows traffic from the SG itself
  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    self      = true
  }

  //allow traffic for specific TCP port
  ingress {
    from_port       = var.db_port
    to_port         = var.db_port
    protocol        = "tcp"
    security_groups = [aws_security_group.db_access_sg.id]
  }

  ingress {
    from_port   = var.db_port
    to_port     = var.db_port
    protocol    = "tcp"
    cidr_blocks = ["${var.jump_server_ip}/32"]
  }

  // outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_kms_key" "database_key" {
  description         = "the encryption key for database ${var.environment}-db-${var.project}"
  key_usage           = "ENCRYPT_DECRYPT"
  is_enabled          = true
  enable_key_rotation = true
}

resource "aws_db_instance" "rds" {
  identifier                          = "${var.environment}-db-${var.project}"
  allocated_storage                   = var.allocated_storage
  storage_encrypted                   = true
  kms_key_id                          = aws_kms_key.database_key.arn
  engine                              = var.db_engine
  engine_version                      = var.db_engine_version
  instance_class                      = var.instance_class
  multi_az                            = var.multi_az
  name                                = var.database_name
  username                            = var.database_username
  password                            = var.database_password
  iam_database_authentication_enabled = var.iam_database_authentication_enabled
  db_subnet_group_name                = aws_db_subnet_group.rds_subnet_group.id
  vpc_security_group_ids              = [aws_security_group.rds_sg.id]
  skip_final_snapshot                 = true
  publicly_accessible                 = var.database_publicly_accessible
  backup_window                       = "07:00-07:45"
  backup_retention_period             = 30
  final_snapshot_identifier           = "${var.environment}-rds-snapshot-${var.project}"

  domain               = var.database_domain
  domain_iam_role_name = var.database_domain_iam_role_name

}
