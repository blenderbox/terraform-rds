# Blenderbox Terraform RDS Module
To use, add the following to your `main.tf` file
```
locals {
  rds_instance_sizes = { # Define different sizes for different environments
    "production" = "db.t3.large"
    "staging"    = "db.t3.large"
    "develop"    = "db.t3.large"
  }
}
module "rds" {
  source                        = "git::ssh://bitbucket.org/blenderbox/terraform-rds"
  environment                   = var.environment
  project                       = var.project
  db_engine                     = var.db_engine
  db_engine_version             = var.db_engine_version
  db_port                       = var.db_port
  jump_server_ip                = var.jump_server_ip
  allocated_storage             = "50"
  database_name                 = null # Database name must be null for mysqlserver-web, set to something like var.project otherwise
  database_username             = var.database_username
  database_password             = var.database_password
  database_publicly_accessible  = false # Set to true if using jumpserver
  database_apply_immediately    = true
  subnet_ids                    = module.networking.private_subnets_ids # use public_subnets_ids
  vpc_id                        = module.networking.vpc_id
  instance_class                = local.rds_instance_sizes[var.environment]
  database_domain_iam_role_name = aws_iam_role.rds_ad_auth.name # Required ONLY if using Active Directory
  database_domain               = aws_directory_service_directory.ad.id # Required ONLY if using Active Directory
  depends_on                    = [aws_iam_role.rds_ad_auth] # Required ONLY if using Active Directory
}

```
